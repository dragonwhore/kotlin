// Сделать программу, которая запускает в фоновом потоке метод печати «World» с задержкой в 1 сек. В основном потоке запускаем печать «Hello,» с задержкой в 2 сек., блокируем поток
fun main() {
    val backgroundThread = Thread {
        Thread.sleep(1000L)
        println("World")
    }

    backgroundThread.start()
    Thread.sleep(2000L)
    println("Hello,")
    backgroundThread.join()
}

// Написать две функции с задержкой, которые будут возвращать 2 числа. В main написать блок кода, который будет суммировать вызов этих 2 функций, сначала написать последовательный вызов функций, вывести сумму и время работы, потом написать блок с асинхронным вызовом и сравнить время работы, обосновать время
import kotlin.system.measureTimeMillis
import kotlinx.coroutines.*

// Функция с задержкой, возвращающая случайное число
suspend fun delayedNumber(number: Int): Int {
    delay(1000) // Задержка в 1 секунду
    return number
}

// Последовательный вызов функций
fun sequentialSum() {
    val time = measureTimeMillis {
        val number1 = delayedNumber(10)
        val number2 = delayedNumber(20)
        println("Sequential sum: ${number1 + number2}")
    }
    println("Time taken: $time ms")
}

// Асинхронный вызов функций
suspend fun asyncSum() {
    val time = measureTimeMillis {
        val deferred1 = async { delayedNumber(10) }
        val deferred2 = async { delayedNumber(20) }
        println("Async sum: ${deferred1.await() + deferred2.await()}")
    }
    println("Time taken: $time ms")
}

fun main() = runBlocking<Unit> {
    sequentialSum()
    asyncSum()
}

// Дополнить код, чтоб программа выводила след текст в консоль.
«I'm sleeping 0 ...I'm sleeping 1 ... I'm sleeping 2 ... main: I'm tired of waiting! I'm running finally main: Now I can quit.»
import kotlin.system.measureTimeMillis
import kotlinx.coroutines.*

// Функция с задержкой, возвращающая случайное число
suspend fun delayedNumber(number: Int): Int {
delay(1000) // Задержка в 1 секунду
return number
}

// Последовательный вызов функций
fun sequentialSum() {
val time = measureTimeMillis {
val number1 = delayedNumber(10)
println("I'm sleeping 0 ...")
val number2 = delayedNumber(20)
println("I'm sleeping 1 ...")
println("I'm sleeping 2 ...")
println("main: I'm tired of waiting!")
println("Sequential sum: ${number1 + number2}")
}
println("main: I'm running finally")
println("main: Now I can quit.")
println("Time taken: $time ms")
}

// Асинхронный вызов функций
suspend fun asyncSum() {
val time = measureTimeMillis {
val deferred1 = async {
delayedNumber(10)
println("I'm sleeping 0 ...")
}
val deferred2 = async {
delayedNumber(20)
println("I'm sleeping 1 ...")
println("I'm sleeping 2 ...")
}
println("main: I'm tired of waiting!")
println("Async sum: ${deferred1.await() + deferred2.await()}")
}
println("main: I'm running finally")
println("main: Now I can quit.")
println("Time taken: $time ms")
}

fun main() = runBlocking<Unit> {
sequentialSum()
asyncSum()
}
